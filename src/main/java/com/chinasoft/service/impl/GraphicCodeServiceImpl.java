package com.chinasoft.service.impl;

import com.chinasoft.model.VerifyCode;
import com.chinasoft.service.GraphicCodeService;
import com.chinasoft.utils.VerifyCodeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


@Service
public class GraphicCodeServiceImpl implements GraphicCodeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphicCodeServiceImpl.class);

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public byte[] getVerificationCode(String deviceId) {

        VerifyCode verifyCode = VerifyCodeUtils.generate(100, 30);
        String code = verifyCode.getCode();
        LOGGER.info("deviceId: {}  ------------------  graphicCode: {}", deviceId, code);

        // 设置Redis缓存数据
        redisTemplate.opsForValue().set(deviceId, code);

        return verifyCode.getImgBytes();
    }
}
