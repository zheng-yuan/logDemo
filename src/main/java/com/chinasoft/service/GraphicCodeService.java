package com.chinasoft.service;


import org.springframework.stereotype.Service;


@Service
public interface GraphicCodeService {

    byte[] getVerificationCode(String deviceId);

}
