package com.chinasoft.controller;

import com.chinasoft.model.Customer;
import com.chinasoft.service.GraphicCodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping({ "/code" })
public class GraphicCodeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GraphicCodeController.class);

    @Autowired
    private GraphicCodeService graphicCodeServer;

    @Autowired
    private HttpServletResponse response;

    /**
     * 获取图形验证码
     * @param deviceId
     * @throws Exception
     */
    @GetMapping("/getVerificationCode")
    public void getVerificationCode(@RequestParam(value = "deviceId", required = true, defaultValue = "100") String deviceId) throws Exception {
        LOGGER.info("deviceId: ---------------------------------------{}", deviceId);

        byte[] imgbyte = graphicCodeServer.getVerificationCode(deviceId);

        // 生成图片存入内存中
        ByteArrayInputStream in = new ByteArrayInputStream(imgbyte);

        //将in作为输入流，读取图片存入image中，而这里in可以为ByteArrayInputStream();
        BufferedImage image = ImageIO.read(in);

        // 将内存中的图片通过流动形式输出到客户端
        response.setHeader("Content-Type", "image/jpeg");

        ImageIO.write(image, "jpg", response.getOutputStream());
    }


    /**
     * 校验图形验证码正确性
     * @param customer
     * @return
     */
    @PostMapping("/checkVerificationCode")
    public String checkVerificationCode(@RequestBody Customer customer) {
        LOGGER.info("deviceId: {}  ========= verificationCode:{}", customer.getDeviceId(), customer.getVerificationCode());


        return "checkVerificationCode";
    }

    /**
     * 判断是否展示图形校验码
     * @return
     */
    @GetMapping("/checkShowCode")
    public String checkShowCode() {


        // 读取配置文件

        return "true";
    }


}
